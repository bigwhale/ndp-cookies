<?php
/*  Copyright 2013 NDP Cookies  (email : David Klasinc <bigwhale@lubica.net>)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 3, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

class ndp_google_analytics {

  public $beta = False;
  public $beta_anon = False;

  public function __construct($ua, $beta, $domain, $noc) {
    $this->ua = $ua;
    $this->beta = $beta;
    $this->domain = $domain;
    $this->noc = $noc;
  }

  public function generate_script() {
    if ($this->beta === '1') {
      $ret = $this->_universal($this->ua, $this->domain);
    } else {
      $ret = $this->_standard($this->ua);
    }
    return $ret;
  }

  private function _standard($ua) {
    if ($ua == '') {
      return '';
    }

    $ret  = "" .
      "<script type=\"text/javascript\">" . 
      "  var _gaq = _gaq || [];" .
      "  _gaq.push(['_setAccount', '" . $ua ."']);" .
      "  _gaq.push(['_trackPageview']);" .
      "    (function() { " .
      "      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;" .
      "      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';" .
      "                 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);" .
      "    })();" .
      "</script>";
    return $ret;
  }

  private function _universal($ua, $domain) {
    if ($ua == '' || $domain == '') {
      return '';
    }
    $ret = "" .
      "<script>" .
      "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){" .
      "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o)," .
      "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)" .
      "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');"; 

      if ($this->noc === '1') {
        $ret .= "ga('create', '" . $ua . "', '" . $domain . "'," .
                  "{'storage': 'none', 'clientId': '" . $this->_generate_id() . "'" . 
                "});";
      } else {
        $ret .= "ga('create', '" . $ua . "', '" . $domain . "');";
      }
      $ret .= "ga('send', 'pageview');" .
      "</script>"; 
      return $ret;
  }

  private function _generate_id() {
    $text = $_SERVER['HTTP_ACCEPT'] .
            $_SERVER['HTTP_ACCEPT_LANGUAGE'] .
            $_SERVER['HTTP_ACCEPT_ENCODING'] .
            $_SERVER['HTTP_USER_AGENT'] . " " .
            $_SERVER['REMOTE_ADDR'];
    return hash("sha256", $text);
  }
}

