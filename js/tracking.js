/*  Copyright 2013 NDP Cookies  (email : David Klasinc <bigwhale@lubica.net>)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 3, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

jQuery(document).ready( function() {

  var url = window.location.pathname;
  var save_choice = "";
  
  if (settings.ndp_master > 0) {
    save_choice = "checked";
  }
  jQuery("<div class=\"ndp-panel\"><div class=\"ndp-inner\">" + settings.ndp_opt_warning +
         "<div class=\"ndp-controls\">" +
         "<input id=\"ndp-allow\" class=\"ndp-button ndp-button ndp-medium ndp-allow\" type=\"button\" value=\"" + settings.ndp_opt_btn_allow + "\">" +
         "<input id=\"ndp-deny\" class=\"ndp-button ndp-button ndp-medium ndp-deny\" type=\"button\" value=\"" + settings.ndp_opt_btn_deny + "\">" +
         "<div class=\"ndp-label\"><input id=\"ndp-check\" class=\"ndp-check\" type=\"checkbox\" value=1 " + save_choice + "><label for=\"ndp-check\">" + settings.ndp_opt_save_choice + "</label></div>" +
         "</div></div></div>").appendTo("body"); 
  if (settings.ndp_panel_show == true) {
    jQuery(".ndp-panel").slideToggle("slow"); 
  }

  jQuery("#ndp-allow").click( function() {
    jQuery("#ndp-check").prop("checked", 1);
    do_post("allow");
  });

  jQuery("#ndp-deny").click( function() {
    do_post("deny");
  });

  function do_post(action) {
    if (jQuery("#ndp-check").is(":checked")) {
      check_state = 1;
    } else {
      check_state = 0;
    }
    jQuery.post(url, { ndp_action: action, ndp_save: check_state });
    jQuery(".ndp-panel").slideToggle("slow"); 
  }

  jQuery(".ndp-panel-toggle").click( function() {
    jQuery(".ndp-panel").slideToggle("slow");
  });
});

