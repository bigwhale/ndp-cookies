NDP-Cookies v0.0.1
===========

NDP Cookies is a WordPress plugin that will try to make your WordPress
installation compliant with EU Cookie regulations, especially for
web sites in Slovenia.

Slovenia will require all web sites to offer opt-in for all the non-
essential cookies. Where 'non-essential' pretty much covers almost
anything that isn't a login session cookie.


Features
--------

 - Opt-in confirmation for cookies that pops up on the first page load.
 - Shortcode for displaying a link which opens already closed pop-up.
 - Blocking 'stock' WordPress cookies that might pose a problem.
 - WordPress function to help theme and plugin developers to imeplement
   a check if cookies are allowed or not.
 - WPML support, all options in plugin are fully translatable and you can
   use separate Google Analytics accounts if you want.
 - Replace YouTube embedded videos with youtube-nocookies variant to prevent
   generating cookies before client clicks play.
 - Silent mode, if you don't enable opt-in confirmation, cookies will never be
   used.


Cookies set by WordPress
------------------------

- Cookies set at login
WordPress will set a bunch of cookies when user logs in. Those are not
the problem. In NDP-Cookies settings you can enter a display message
which informs user that by logging in they agree to accept all the cookies.

- Cookies set when commenting on a post
Three cookies will be set when user posts a comment. Displaying a warning
about cookies could be a solution in this case, however, with strict
interpretation of 'Slovenian cookie law' this could pose a problem.
NDP-Cookies plugin will disable Wordpress comment cookies.

- Cookies set by JetPack Stats module
If you install JetPack from Wordpress.com and turn on Stats tracking, you'll
end up with a couple of cookies. These are also removed when you enable
NDP-Cookies

- Cookies set by YouTube when embedding video clips
When you embed YouTube videos a bunch of cookies will be set by YouTube.
NDP-Cookies will rewrite YouTube output and use youtube-nocookie.com instead
of youtube.com to embed videos. Cookies are set only when user plays the
content.

- There are probably more, but not all of them are covered here, so if
you notice a stray cookie, eat it. ;) And do let me know where you found it.


Support for Google Analytics
----------------------------

Everyone wants it. Well, allmost everyone. NDP-Cookies will give you several
options on how to use Google Analytics.

 - Standard Google Analytics
 Nothing new here, disabled if client decided not to opt-in for cookies.

 - Google Universal Analytics Beta
 Use the new, beta, google analytics. Check with Google what's new. The
 main difference is that only one cookie is set.

 - Google Universal Analytics Beta without cookies
 Universal Analytics can work in no-cookies mode. In this case NDP-Cookies
 will generate a semi-unique client ID and pass it to Google. No cookies
 are stored on clients computer.


Limitations
-----------

Because of a strict opt-in policy, first time users will never register
in Google Analytics or JetPack Stats. When cookies are allowed an AJAX
request is made to register this. Every other page load will be normal.

In the future releases I might do some JavaScript magic and try to get
around this problem.

WPML will always set a language cookie. This can't be disabled without
a tiny little hack in the WPML itself. Find sitepress.class.php file
and search for setcookie() call. Then simply wrap it in an if check
and with the help of ndp\_cookie\_check() function. Example

    if (function_exists('ndp_cookie_check') && ndp_cookie_check()) {
      setcookie('_icl_current_language',
                $this->get_current_language(),
                time()+86400, $cookie_path, $cookie_domain);
    } else {
      setcookie('_icl_current_language',
                '', time() - 1000, $cookie_path, $cookie_domain);
    }


This will remove the cookie if user has decided to disable cookies.

Known bugs
----------

Except for the typos in this file, they are still in hiding. ;)


DISCLAIMER
----------

This plugin will block only cookies that were mentioned here. Make sure
you check your WordPress installation for other cookies. Cookies can be
set by themes, plugins or third party sites which code you include in
your web site.

    rgrep setcookies *    # is your friend ;)

Author of this plugin is not responsible if you missed any of those or if
you didn't check your site before going public. ;)

