<?php
  /*  Copyright 2013 NDP Cookies  (email : David Klasinc <bigwhale@lubica.net>)

      This program is free software; you can redistribute it and/or modify
      it under the terms of the GNU General Public License, version 3, as
      published by the Free Software Foundation.

      This program is distributed in the hope that it will be useful,
      but WITHOUT ANY WARRANTY; without even the implied warranty of
      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
      GNU General Public License for more details.

      You should have received a copy of the GNU General Public License
      along with this program; if not, write to the Free Software
      Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
   */

/*
  Plugin Name: NDP Cookies
  Plugin URI: http://www.twm-kd.com/
  Description: Disable Wordpress cookies by default and display opt-in warning notice
  Version: 0.0.1
  Author: David Klasinc
  Author URI: http://www.twm-kd.com/
  License: GPLv3 or later
*/

define('DONOTCACHEPAGE', true);

require_once('ndp-settings.php');
require_once('ndp-google-analytics.php');

$ndp_options = new ndp_options(); // Get the options working

add_action('wp_enqueue_scripts', 'ndp_tracking_script');  // Take care of google analytics
add_action('admin_enqueue_scripts', 'ndp_admin_script');  // Take care of admin interface

add_action('plugins_loaded', 'ndp_override_cb');          // Intercept AJAX post calls
add_action('plugins_loaded', 'ndp_master_check');         // Master check

add_shortcode('ndp-cookie-panel', 'ndp_open_panel');      // Open cookie panel shortcode
add_filter('login_message', 'ndp_login_message');         // Custom login message
add_action('wp_head', 'ndp_wp_head');                     // Inject analytics scripts

add_filter('embed_oembed_html', 'ndp_embed_youtube_cb');  // Rewrite youtube calls


function ndp_master_check() {
  $ndp_master = 0;
  if (isset($_COOKIE['ndp-check'])) {
    $ndp_master = $_COOKIE['ndp-check'];
  }

  if ( is_user_logged_in() ) {
    $ndp_master = 2;
  }

  // We should use this, instead of a global variable, right?
  if (get_option('ndp_master_switch') === FALSE) {
    add_option('ndp_master_switch', $ndp_master);
  } else {
    update_option('ndp_master_switch', $ndp_master);
  }

/* Perform ugly WP hacks
   2 - cookies are allowed
   1 - cookies are forbidden, except the cookie about cookies being forbidden
   0 - cookies are forbidden, all of them
*/

  switch ($ndp_master) {
    case 2:
      break;
    case 1:
    default:
      // Removing comment cookies
      remove_action('set_comment_cookies', 'wp_set_comment_cookies', 10, 2);

      // WPML will store language in a cookie, for no particular reason. Let's just nuke it.
      setcookie('_icl_current_language', '', time() - 1000);

      // Take care of really weird stuff, WordPress.com JetPack Stats for example
      add_action('wp_loaded', 'ndp_additional_checks');
      break;
  }
}

function ndp_wp_head() {
  $master = get_option('ndp_master_switch');
  $ua = get_option('ndp_opt_google_ua');
  $bua = get_option('ndp_opt_google_bua');
  $domain = get_option('ndp_opt_google_domain');
  $noc = get_option('ndp_opt_google_noc');
  $ndp_analytics = new ndp_google_analytics($ua, $bua, $domain, $noc);
  $script = "";

  switch ($master) {
    case 2:
      $script = $ndp_analytics->generate_script();
      break;
    default:
      if ($bua && $noc) {
        $script = $ndp_analytics->generate_script();
      }
  }

  if ($script != '') {
    echo $script;
  }
}

function ndp_tracking_script() {
  if (($ndp_master = get_option('ndp_master_switch')) === FALSE) {
    $ndp_master = 0;
  }

  $js_opts = array('ndp_opt_warning' => get_option('ndp_opt_warning'),
                   'ndp_opt_btn_allow' => get_option('ndp_opt_btn_allow'),
                   'ndp_opt_btn_deny' => get_option('ndp_opt_btn_deny'),
                   'ndp_opt_save_choice' => get_option('ndp_opt_save_choice'),
                   'ndp_master' => $ndp_master,
                   'ndp_panel_show' => false
                  );
  if ($ndp_master == 0 && get_option('ndp_opt_enabled') == 1) {
    $js_opts['ndp_panel_show'] = true;
    wp_register_style('ndp-tracking-style', plugins_url('css/style.css', __FILE__));
    wp_enqueue_style('ndp-tracking-style');
    wp_enqueue_script('ndp-tracking-script', plugins_url('js/tracking.js', __FILE__), array('jquery'));
    wp_localize_script('ndp-tracking-script', 'settings', $js_opts);
  }


}

function ndp_admin_script($hook) {
  if ($hook == 'settings_page_ndp-tracking-options') {
    wp_register_style('ndp-tracking-style', plugins_url('css/style.css', __FILE__));
    wp_enqueue_style('ndp-tracking-style');
    wp_enqueue_script('ndp-tracking-script', plugins_url('js/admin.js', __FILE__), array('jquery'));
  }
}

// Intercept POSTs and check if they are for us.
// Intercepting stuff right after all plugins are loaded.
function ndp_override_cb() {
  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (isset($_POST['ndp_action']) && isset($_POST['ndp_save'])) {
      if ($_POST['ndp_action'] == 'allow') {
        setcookie('ndp-check', '2', time() + (10 * 365 * 24 * 60 * 60), '/');
      } elseif ($_POST['ndp_action'] == 'deny' && $_POST['ndp_save'] == 1) {
        setcookie('ndp-check', '1', time() + (10 * 365 * 24 * 60 * 60), '/');
      } elseif ($_POST['ndp_action'] == 'deny') {
        setcookie('ndp-check', '' , time() - 1000, '/');
      }
    die();
    }
  }
}

// Create shortcode for opening cookie panel
function ndp_open_panel($attributes) {
  $ret = "<a href=\"#\" class=\"ndp-panel-toggle\">";
  $ret .= get_option('ndp_opt_panel_link');
  $ret .= "</a>";
  return $ret;
}

// Add a login message warning
function ndp_login_message($msg) {
  $custom = get_option('ndp_opt_login_message');
  if ($custom && $custom != '') {
    $msg .= '<p class="ndp-login-message">' . $custom . '</p>';
  }
  return $msg;
}

function ndp_cookie_check() {
  $master = get_option('ndp_master_switch');
  if ($master == 2) {
    return TRUE;
  } else {
    return FALSE;
  }
}

function ndp_embed_youtube_cb($html) {
  $html = preg_replace('/src=\"(http|https)\:\/\/www\.youtube\.com/', 'src="//www.youtube-nocookie.com', $html);
  return $html;
}

function ndp_additional_checks() {
  // Hardcore block of JetPack stats tracking cookies
  if (function_exists('stats_load')) {
    remove_action( 'template_redirect', 'stats_template_redirect', 1 );
    remove_action( 'wp_head', 'stats_admin_bar_head', 100 );
    remove_action( 'wp_head', 'stats_hide_smile_css' );
    remove_action( 'jetpack_admin_menu', 'stats_admin_menu' );
    remove_action( 'wp_dashboard_setup', 'stats_register_dashboard_widget' );
  }
}

