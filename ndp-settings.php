<?php
/*  Copyright 2013 NDP Cookies  (email : David Klasinc <bigwhale@lubica.net>)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 3, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

class ndp_options {

  public $ndp_settings = array();

  public function __construct() {
    if ( is_admin() ) {
      add_action('admin_menu', array($this, 'admin_menu'));
      add_action('admin_init', array($this, 'admin_init'));
    }
  }

  public function admin_menu() {
    add_options_page('NDP Cookies Options',
                     'NDP Cookies',
                     'manage_options',
                     'ndp-tracking-options',
                     array($this, 'tracking_options_cb'));
  }


  public function admin_init() {
      array_push($this->ndp_settings,
           // option db id              option id            translation id              label                                callback
        array('ndp_opt_enabled',        'ndp_enabled',       'Cookie warning label',     'Enable cookie warning',              'enable_cb'),
        array('ndp_opt_google_ua',      'ndp_google_ua',     'Google Analytics label',   'Google Analytics',                   'google_ua_cb'),
        array('ndp_opt_google_bua',     'ndp_google_bua',    'Google Universal label',   'Universal Analytics',                'google_bua_cb'),
        array('ndp_opt_google_domain',  'ndp_google_domain', 'Google Analytics domain',  'Universal Analytics domain',         'google_domain_cb'),
        array('ndp_opt_google_noc',     'ndp_google_noc',    'Google Analytics cookie',  'Disable Universal Analytics cookie', 'google_noc_cb'),
        array('ndp_opt_warning',        'ndp_warning',       'Front page warning label', 'Front page warning',                 'warning_cb'),
        array('ndp_opt_btn_allow',      'ndp_btn_allow',     'Allow button label',       'Allow button text',                  'btn_allow_cb'),
        array('ndp_opt_btn_deny',       'ndp_btn_deny',      'Deny button label',        'Deny button text',                   'btn_deny_cb'),
        array('ndp_opt_save_choice',    'ndp_save_choice',   'Save settings label',      'Save settings text',                 'save_choice_cb'),
        array('ndp_opt_panel_link',     'ndp_panel_link',    'Panel shortcode label',    'Panel shortcode text',               'panel_link_cb'),
        array('ndp_opt_login_message',  'ndp_login_message', 'Login message label',      'Login message',                     'login_message_cb')
      );
    add_settings_section('ndp_cookie_settings',                     // ID
                         $this->i('Admin section name', 'General'), // Title
                         array($this, 'settings_section_cb'),       // Callback
                         'ndp-admin');                              // Page

    register_setting('ndp-settings-group',               // Settings group
                     'ndp-settings',                     // Name of the option to save
                     array($this, 'settings_sanitize')); // Sanitize callback

    foreach ($this->ndp_settings as $i) {
      add_settings_field($i[1], $this->i($i[2], $i[3]), array($this, $i[4]), 'ndp-admin', 'ndp_cookie_settings');
    }

  }

  public function settings_section_cb() {
    echo $this->i('Admin section desc', 'Enter your setting below:');
  }

  public function settings_sanitize ($input) {

    foreach ($this->ndp_settings as $i) {
      switch ($i[0]) {
        case 'ndp_opt_enabled':
          if (array_key_exists('ndp_enabled', $input)) {
            $input['ndp_enabled'] = 1;
          } else {
            $input['ndp_enabled'] = 0;
          }
          break;
        case 'ndp_opt_google_bua':
          if (array_key_exists('ndp_google_bua', $input)) {
            $input['ndp_google_bua'] = 1;
          } else {
            $input['ndp_google_bua'] = 0;
          }
          break;
        case 'ndp_opt_google_noc':
          if (array_key_exists('ndp_google_noc', $input)) {
            $input['ndp_google_noc'] = 1;
          } else {
            $input['ndp_google_noc'] = 0;
          }
          break;
         default:
          if (!array_key_exists($i[1], $input)) {
            $input[$i[1]] = '';
          }
          break;
      }

      if (get_option($i[0]) === FALSE) {
        add_option($i[0], $input[$i[1]]);
      } else {
        update_option($i[0], $input[$i[1]]);
      }
    }
    return $input;
  }

  public function enable_cb() {
    $checked = get_option('ndp_opt_enabled');
    if ($checked === '1') {
      echo "<input type=\"checkbox\" id=\"ndp_enabled\" name=\"ndp-settings[ndp_enabled]\" value=\"1\" checked>";
    } else {
      echo "<input type=\"checkbox\" id=\"ndp_enabled\" name=\"ndp-settings[ndp_enabled]\" value=\"0\">";
    }
  }

  public function google_ua_cb() {
    echo "<input type=\"text\" id=\"ndp_google_ua\" name=\"ndp-settings[ndp_google_ua]\" value=\"" . get_option('ndp_opt_google_ua') . "\">";
  }

  public function google_domain_cb() {
    echo "<input type=\"text\" id=\"ndp_google_domain\" name=\"ndp-settings[ndp_google_domain]\" value=\"" . get_option('ndp_opt_google_domain') . "\">";
  }

  public function google_bua_cb() {
    $checked = get_option('ndp_opt_google_bua');
    if ($checked === '1') {
      echo "<input type=\"checkbox\" id=\"ndp_google_bua\" name=\"ndp-settings[ndp_google_bua]\" value=\"1\" checked>";
    } else {
      echo "<input type=\"checkbox\" id=\"ndp_google_bua\" name=\"ndp-settings[ndp_google_bua]\" value=\"0\">";
    }
  }

  public function google_noc_cb() {
    $checked = get_option('ndp_opt_google_noc');
    if ($checked === '1') {
      echo "<input type=\"checkbox\" id=\"ndp_google_noc\" name=\"ndp-settings[ndp_google_noc]\" value=\"1\" checked>";
    } else {
      echo "<input type=\"checkbox\" id=\"ndp_google_noc\" name=\"ndp-settings[ndp_google_noc]\" value=\"0\">";
    }
  }

  public function warning_cb() {
    echo "<textarea class=\"ndp-settings\" id=\"ndp_warning\" name=\"ndp-settings[ndp_warning]\">";
      echo get_option('ndp_opt_warning');
    echo "</textarea>";
  }

  public function btn_allow_cb() {
    if (($val = get_option('ndp_opt_btn_allow')) === FALSE)
    {
      $val = $this->i('Default value', 'Allow');
    }

    echo "<input type=\"text\" id=\"ndp_btn_allow\" " .
         "name=\"ndp-settings[ndp_btn_allow]\" value=\"" .
         $val .
         "\">";
  }

  public function btn_deny_cb() {
    if (($val = get_option('ndp_opt_btn_deny')) === FALSE)
    {
      $val = $this->i('Default value', 'Deny');
    }

    echo "<input type=\"text\" id=\"ndp_btn_deny\" " .
         "name=\"ndp-settings[ndp_btn_deny]\" value=\"" .
         $val .
         "\">";
  }

  public function save_choice_cb() {
    if (($val = get_option('ndp_opt_save_choice')) === FALSE)
    {
      $val = $this->i('Default value', 'Save settings');
    }

    echo "<input type=\"text\" id=\"ndp_save_choice\" ".
         "name=\"ndp-settings[ndp_save_choice]\" value=\"" .
         $val .
         "\">";
  }

  public function panel_link_cb() {
    if (($val = get_option('ndp_opt_panel_link')) === FALSE)
    {
      $val = $this->i('Default value', 'Open cookies settings');
    }

    echo "<input type=\"text\" id=\"ndp_panel_link\" ".
         "name=\"ndp-settings[ndp_panel_link]\" value=\"" .
         $val .
         "\">";
  }

  public function login_message_cb() {
    echo "<textarea class=\"ndp-settings\" id=\"ndp_login_message\" name=\"ndp-settings[ndp_login_message]\">";
      echo get_option('ndp_opt_login_message');
    echo "</textarea>";
  }

  public function tracking_options_cb() {
    ?>
    <div class="wrap">
      <?php screen_icon(); ?>
      <h2><?php echo _('Customize your warnings on cookies'); ?></h2>
      <form method="post" action="options.php">
        <?php
            // This prints out all hidden setting fields
            settings_fields('ndp-settings-group');
            do_settings_sections('ndp-admin');
        ?>
        <?php submit_button(); ?>
      </form>
    </div>
    <?php
  }

  private function i($name, $text) {
    if (function_exists('icl_register_string')) {
      icl_register_string('NDP Cookies', $name, $text);
      return icl_t('NDP Cookies', $name, $text);
    } else {
      return $text;
    }
  }

}  /* ndp_options class */

